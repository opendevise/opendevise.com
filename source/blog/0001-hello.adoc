= Documentation Ideas Coming Your Way!
OpenDevise
:revdate: 2017-11-16 08:45
:description: Welcome to the OpenDevise blog. \
We'll use this space to share our documentation ideas, insights, and experiences as well as news about our company and products.
:keywords: OpenDevise, mission, documentation, Antora, Asciidoctor
:page-tags: News
:page-image: opendevise-ideas-feature.png
:page-featured:
:uri-antora: https://gitlab.com/antora
:uri-asciidoctor: http://asciidoctor.org

Hello, documentation world!
Welcome to the Docs Dispatch, our blog about all things documentation.

We've revved up the blog so we can share our documentation ideas, insights, and experiences with you.
We'll cover it all, from writing to team workflows all the way through to continuous delivery, whether it's for one product or a score of them.
You'll also learn about Open Source tools we're working on to help make this all possible.

First, a little bit about who we are.
We're an experienced team of writers, system architects, and developers.
We put that experience to work to improve the state of documentation.
But not just any documentation.
Documentation written in the lightweight markup language AsciiDoc and processed with our favorite tool, {uri-asciidoctor}[Asciidoctor].

Although we're a small company, we have a big goal.
We want writers to love writing documentation and be productive creating it.
We want readers to love reading documentation and learn effectively from it.
And, above all, we want to give documentation managers the creative space to launch new content intiatives.

> We help organizations produce documentation that their users adore.

We're kicking off this blog with a series of articles about {uri-antora}[Antora], a new documentation tool we're cooking up.
We'll lay out the thought process that led us to creating Antora and why we feel strongly that there's a need for it.
In brief, while there are countless site generators out there, few are well-suited for documentation, and none are made exclusively for AsciiDoc.
Antora is designed exactly for that use case.

We believe Antora is going to have a big impact on documentation and become an integral part of the Asciidoctor ecosystem.
We even plan to use it for the Asciidoctor documentation.
You'll learn a lot more about Antora in the articles to come.

Thanks for stopping by!
We hope you'll enjoy our musings about all things documentation.
