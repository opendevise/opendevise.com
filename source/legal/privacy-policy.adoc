= Privacy Policy
:revdate: March 20, 2018
:page-layout: document
:page-return: { id: legal/index, content: Legal }

This Privacy Policy explains and governs how data gathered by, and submitted to, our Sites and Services is used and processed.

== Information We Collect and Use From Website Visitors

Certain visitors to our websites choose to interact with our Sites and Services in ways that require us to gather personal information.
The amount and type of information that we gather depends on the nature of the interaction.

For example, we ask visitors who want to receive updates from OpenDevise to provide an email address for that purpose.
Similarly, visitors who engage in transactions with OpenDevise, such as inquiring about our Services, may be asked to provide additional types of information.
The types of personal information we may collect include:

* Contact information, such as name, email address, or phone number;
* Information about your business, such as company name and your role at the company.

In each case, we collect such information only insofar as is necessary or appropriate to fulfill the purpose of the visitor’s interaction with OpenDevise.

You can refuse to supply personal information, but it may prevent you from using certain features or our Sites or Services.

=== Cookies and Tracking

We may use cookies or similar technologies to analyze trends, administer the Sites, track how people use the Sites, and to gather demographic information about our visitor base.
You can control the use of cookies at the individual browser level.
If you choose to disable cookies, it may limit your use of certain features or functions on our Sites.

As is true of most websites, we gather certain information automatically.
This information may include Internet protocol (IP) addresses, browser type, Internet service provider (ISP), referring/exit pages, the pages viewed on our Sites, operating system, and tracking of specific behavior on the Sites.

== Information Collected from Other Sources

With your authorization, we may also obtain information about you from other sources.
For example, if you submit a new issue to one of the open source projects we maintain on GitLab or GitHub, we may receive information from that service (such as your profile information and repository metrics).
The information we receive depends on which services you authorize and any options that are available.

== How We Use the Information We Collect

=== Communication

If you have supplied your email address to OpenDevise, we may occasionally send you an email to tell you about new features, solicit your feedback, or keep you up-to-date with what’s going on with OpenDevise.
However, we primarily use our blogs or social media channels to communicate this information, so we expect to keep this type of email to a minimum.

If you send us a request, such as a support ticket, email, or other feedback, we reserve the right to record or publish it in order to help us clarify or respond to your request or to help us support other users.

=== Operations

We do not share personal information with anyone other than our employees, contractors, and affiliated organizations.
OpenDevise discloses personal information only to those of its employees, contractors, and affiliated organizations
(i) that need to know that information in order to process it on our behalf or to provide our Services, and
(ii) that have agreed to not disclose it to others.
Some of our employees, contractors, and affiliates may be located outside your home country; by agreeing to our xref:terms-of-use.adoc[Terms of Use], you consent to the transfer of such information to them.

=== Non-personal Information

We may use internally and publicly share non-personal information (e.g., anonymous, aggregated data) without restriction.
For example, we may publish reports on website usage based on this data.

== Legal Compliance

We don't disclose personal information with anyone other than our employees, contractors and affiliated organizations as described in "`How We Use the Information We Collect`" or when required by law in certain situations.
OpenDevise discloses personal information only in response to a subpoena, court order, or other lawful government request when we believe in good faith that disclosure is necessary to protect our rights, or protect your safety or the safety of others.

== Business Transfers

If OpenDevise is acquired or merges with another company, or some or substantially all of our assets, are acquired, or in the unlikely event that OpenDevise goes out of business or enters bankruptcy, user information could be one of the assets that is transferred or acquired by a third party.
You acknowledge that such transfers may occur, and that any acquirer of OpenDevise may continue to use your personal information as set forth in this Privacy Policy.

== Information Storage and Retention

We take all measures reasonably necessary to protect against the unauthorized access, use, alteration, or destruction of the personal information submitted to us, both during transmission and once it is received.
However, no electronic data transmission or storage of information can be guaranteed to be 100% private and secure, so we can not ensure or warrant the privacy or security of the information we collect from you.

We may retain your personal information for as long as your account is active, or as otherwise needed to provide you Services, comply with our legal obligations, resolve disputes and enforce our agreements.

If you are under 13, we don't want your personal information, and you must not provide it to us.
If we become aware that a child under 13 has provided us with personal information, we will take steps to delete it.

== Access and Choice

Upon request, we will provide you with information about whether we hold any of your personal information.
You may access, correct, or request deletion of your personal information by contacting us at \legal@opendevise.com.

== Privacy Policy Changes

OpenDevise may change this Privacy Policy from time to time and will note the revision date on this page.
If the changes are substantive, we will announce the update through an online channel such as blog posts, notices, or emails.
Your continued use of the OpenDevise Sites or Services after the effective date of any changes constitutes your acceptance of such changes.
