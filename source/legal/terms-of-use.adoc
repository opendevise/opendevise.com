= Terms of Use
:revdate: March 29, 2018
:sectnums:
:page-layout: document
:page-return: { id: legal/index, content: Legal }

These Terms of Use ("`Terms`") explain your rights and responsibilities when you use any of our Sites and Services.

By accessing, browsing, or using our Sites or Services, you automatically agree to these Terms and our xref:privacy-policy.adoc[Privacy Policy].
If you are agreeing to the Terms on behalf of a company, organization, or other legal entity, you represent and warrant that you are authorized to do so.

== Who We Are

Our legal name is OpenDevise Inc.
We're a Colorado company based in the City of Lone Tree.
When we use the words "`we`", "`us`", "`our`", and "`OpenDevise`", we're referring to OpenDevise Inc.
These Terms are a binding agreement between you and OpenDevise.

== Description of Our Sites, Software, and Services

"`Sites`" means each of the websites owned and operated by OpenDevise, as well as related source repositories, chat rooms, feeds, social media, user-generated content, messages, notifications, newsletters, and emails.
Our Sites include multiple domains, such as opendevise.com, antora.org, and docs.antora.org.
Some of our Sites connect you with links to websites that are provided by other parties and are subject to separate terms and/or privacy policies.

"`Software`" means the source code, objects, configuration files, APIs, ASTs, models, packages, binaries, archives, and other such materials, together with the related documentation, that OpenDevise creates, maintains, and publishes.

"`Services`" means our professional services such as migration and custom development, DocOps, and technical content consulting, as well as our support subscriptions.
Our Services are governed by additional terms that become part of your agreement with us when you purchase our Services.

== Account Registration

You may need to register for an account in order to access additional features of the Sites or Services.
When registering, you may be allowed to create a username and/or password.

You are responsible for maintaining the confidentiality of your password and are solely responsible for all activities resulting from the use of your password and activities conducted through your account.

== Content Licensing

Our Sites and Services include content such as articles, blog entries, testimonials, source code, software descriptions and documentation, information, data, news articles, graphics, images, photographs, audio and video clips, user-generated comments, and other materials (collectively, "`Content`").

The Content is authored and licensed by OpenDevise, individual contributors to the Sites, and other sources.
Where possible, the footer of the Sites will display a notice with the applicable license.
Some Content is acquired from sources that prohibit further use of their Content without advance permission.
You agree to abide by such notices.

== Content Submissions

We may provide opportunities for you to submit Content to some of our Sites, such as commenting on a blog post, providing feedback on an issue, or contributing text, diagrams, or photographs (each a "`Content Submission`").
Unless your Content Submission is made under a separate agreement with OpenDevise, in which case that agreement will govern, then for all Content Submissions, you agree to the following in connection with each:

* You represent and warrant that your Content Submission will comply with these Terms and any additional terms that may govern your Content Submission.
* You grant us a nonexclusive, royalty-free, worldwide, sublicensable (to those we work with) license to use your Content Submission in connection with OpenDevise’s mission, Software, Services, and Sites.
* You acknowledge that your Content Submissions may be accessible by other registered users of the Sites or the public.
* If your Content Submission contains source code or materials related to our Software, you agree to license it in a manner that is compatible with the Software.
* You represent and warrant that you have the rights necessary to grant the rights granted herein, and further, that the uses contemplated under these Terms will not infringe the proprietary or intellectual property rights of any third party.
* You understand and agree that OpenDevise reserves the right, at our discretion, to review, modify, or remove any Content Submission that we deem is objectionable or in violation of these Terms.

== Software Licensing

Any Software that is made available to you to download from our Sites or Services is the copyrighted work of OpenDevise and the individual contributors to the Software.
Your use of the Software is governed by the terms of the license ("`License Agreement`") which accompanies or is included with the Software.
You will not install any Software that is accompanied by or includes a License Agreement, unless you first agree to the terms of the License Agreement.

Software authored by OpenDevise and its contributors is generally made available under the Mozilla Public License (MPL).
Please see the applicable source code, software package, GitLab repository, or GitHub repository for information on the specific License Agreement.

SOFTWARE IS WARRANTED, IF AT ALL, IN ACCORDANCE WITH THE TERMS OF THE LICENSE AGREEMENT.
EXCEPT AS SET FORTH IN THE LICENSE AGREEMENT, ALL EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT, ARE DISCLAIMED, EXCEPT TO THE EXTENT THAT SUCH DISCLAIMERS ARE HELD TO BE LEGALLY INVALID.

== Software Submissions

You may contribute source code, such as snippets, scripts, helpers, libraries, templates, stylesheets, objects, tests, extensions, tools, and components, as well as Content related to the Software and the Software's documentation (each a "`Software Submission`").
Unless your Software Submission is made under a separate agreement with OpenDevise, in which case that agreement may govern, then for all Software Submissions, you agree to the following in connection with each:

* You agree to license your Software Submission under the terms of the corresponding license of the Software to which you are contributing.
Please see the applicable source code, software package, GitLab repository, or GitHub repository for information on the specific license.
* You represent and warrant that your Software Submission will comply with these Terms and any additional terms that may govern your Software Submission.
* You grant us a nonexclusive, royalty-free, worldwide, sublicensable (to those we work with) license to use your Software Submission in connection with OpenDevise’s mission, Software, Services, and Sites.
* You acknowledge that your Software Submission may be accessible by other registered users of the applicable Sites or the public.
* You represent and warrant that you have the rights necessary to grant the rights granted herein, and further, that the uses contemplated under these Terms will not infringe the proprietary or intellectual property rights of any third party.
* You understand and agree that OpenDevise reserves the right, at our discretion, to review, modify, or remove any Software Submission that we deem is objectionable or in violation of these Terms.

== Your Use of the Sites and Services

Please do not use the Sites or Services in a way that violates any laws, infringes on anyone’s rights, is offensive, or interferes with the Sites or Services or any features on the Sites or Services.
If we (in our sole discretion) determine that you have acted inappropriately, we reserve the right to take down Content, terminate your account, prohibit you from using the Sites or Services, and take appropriate legal actions.

When you use the Sites or Services or send communications to us through the Sites or Services, you are communicating with us electronically.
You consent to receive electronically any communications related to your use of the Sites or Services.
We may communicate with you by email or by posting notices on the Sites.
You agree that all agreements, notices, disclosures and other communications that are provided to you electronically satisfy any legal requirement that such communications be in writing.
All notices from us intended for receipt by you shall be deemed delivered and effective when sent to the email address you provide to us.

Please note that by providing Content Submissions, creating a user account or otherwise providing us with your email address, postal address or phone number, you are agreeing that we may contact you at that address or number in a manner consistent with our Privacy Policy.
Our xref:privacy-policy.adoc[Privacy Policy] describes how we handle information that we receive from you in connection with our Sites and Services.

== Intellectual Property

Using our Sites or Services does not give you ownership of any intellectual property rights to the Content, Services, or Software you access.

If you believe any Content on the Sites infringes on your copyrights, you may request that we remove the Content from the Sites.
Contact us at \legal@opendevise.com.

The trademarks, logos, and service marks (collectively, "`Marks`") displayed on the Sites are the property of OpenDevise or other third parties.
You are not permitted to use these Marks without the prior written consent of OpenDevise or such third party which may own the Mark.

== Community Networks

The Sites and Services may include features that operate in conjunction with certain third party social networking websites that you visit such as Gitter, Vimeo, and Twitter, as well as third party version control system websites that you visit such as GitLab and GitHub ("`Community Network Features`").
While your use of the Community Network Features is governed by these Terms, your access and use of third party social networking and version control system websites and the services provided through our Sites and Services is governed by the terms and other agreements posted on the third party websites.
You are responsible for ensuring that your use of those websites complies with any applicable terms, policies, or other agreements.

== Modifications and Termination

We reserve the right to modify our Sites and Services at any time, with or without notice to you.
For example, we may add or remove a particular feature.
You can choose to stop using our Sites and, if applicable, deleting your account, at any time for any reason.

== Indemnification

You agree to defend, indemnify and hold harmless OpenDevise, its contributors, affiliates, contractors, licensors, and partners; and the respective directors, officers, shareholders, employees and agents of the foregoing (collectively, "`Indemnified Parties`") from and against any and all claims, losses, liabilities, and expenses, including attorneys' fees, arising out of or related to your use of the OpenDevise Sites or Services including, but not limited to, from your Content or Software Submissions or from your violation of any of these Terms.

== Disclaimer; Limitation of Liability

THE OPENDEVISE SITES AND ALL CONTENT, MATERIALS, INFORMATION, SOFTWARE, PRODUCTS, AND SERVICES ARE PROVIDED "`AS IS`" WITH ALL FAULTS.
TO THE EXTENT PERMITTED BY LAW, OPENDEVISE AND THE INDEMNIFIED PARTIES HEREBY DISCLAIM ALL WARRANTIES, WHETHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION WARRANTIES THAT THE OPENDEVISE SITES AND SERVICES ARE FREE OF DEFECTS, MERCHANTABLE, FIT FOR A PARTICULAR PURPOSE, AND NON-INFRINGING.
YOU BEAR THE ENTIRE RISK AS TO USING THE OPENDEVISE SITES AND SERVICES FOR YOUR PURPOSES AND AS TO THE QUALITY AND PERFORMANCE OF THE OPENDEVISE SITES AND SERVICES, INCLUDING WITHOUT LIMITATION THE RISK THAT YOUR HARDWARE, SOFTWARE, OR CONTENT IS DELETED OR CORRUPTED, THAT SOMEONE ELSE GAINS UNAUTHORIZED ACCESS TO YOUR INFORMATION, OR THAT ANOTHER USER MISUSES OR MISAPPROPRIATES YOUR CONTENT OR SOFTWARE SUBMISSION.
THIS LIMITATION WILL APPLY NOTWITHSTANDING THE FAILURE OF ESSENTIAL PURPOSE OF ANY REMEDY.
SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF IMPLIED WARRANTIES, SO THIS DISCLAIMER MAY NOT APPLY TO YOU.

EXCEPT AS REQUIRED BY LAW, OPENDEVISE AND THE INDEMNIFIED PARTIES WILL NOT BE LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, CONSEQUENTIAL, OR EXEMPLARY DAMAGES ARISING OUT OF OR IN ANY WAY RELATING TO THESE TERMS OR THE USE OF OR INABILITY TO USE THE OPENDEVISE SITES AND SERVICES, INCLUDING WITHOUT LIMITATION DIRECT AND INDIRECT DAMAGES FOR LOSS OF GOODWILL, WORK STOPPAGE, LOST PROFITS, LOSS OF DATA, AND COMPUTER FAILURE OR MALFUNCTION, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND REGARDLESS OF THE THEORY (CONTRACT, TORT, OR OTHERWISE) UPON WHICH SUCH CLAIM IS BASED.
THE COLLECTIVE LIABILITY OF OPENDEVISE AND THE INDEMNIFIED PARTIES UNDER THIS AGREEMENT WILL NOT EXCEED $100 (ONE HUNDRED DOLLARS).
SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL, CONSEQUENTIAL, OR SPECIAL DAMAGES, SO THIS EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU.

== More Details

We may update these Terms from time to time to address a new feature or to clarify a provision.
The updated Terms will be posted online.
If the changes are substantive, we will announce the update through an online channel such as blog posts, notices, or emails.
Your continued use of the OpenDevise Sites or Services after the effective date of such changes constitutes your acceptance of such changes.

Some of our Sites connect you with links to websites that are provided by other parties and are subject to separate terms and/or privacy policies.
We are not responsible for the content or availability of the linked websites.
The inclusion of any link to a website does not imply endorsement by OpenDevise of the website or their entities, products or services.

If you do not comply with these Terms, and we don't take action right away, that doesn't mean we are giving up any rights that we may have.

These Terms are governed by the laws of the state of Colorado, U.S.A., excluding its conflict of law provisions.
All claims and disputes arising out of the OpenDevise Sites, Services, or these Terms shall be brought exclusively in the courts of Douglas County, Colorado, and you consent to personal jurisdiction in those courts.

If any portion of these Terms is held to be invalid or unenforceable, the remaining portions will remain in full force and effect.

Additional terms, conditions, and agreements may apply to specific Content, Software, or Services offered through the Sites.
Should a conflict arise, such additional terms, conditions, and agreements will prevail over these Terms.

== Questions

If you have any questions about these Terms, please contact us at \legal@opendevise.com or by mail at OpenDevise Inc., 10020 Trainstation Circle #425, Lone Tree, CO 80124, USA.
